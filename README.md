# NodeJS + Twitter Stream API

This project allows, with NodeJS, to access Twitter's Stream API.
Initialy, we create a stream on those subjects : "France", "Cat" and "Trump".
On the client side, filter buttons allow use to make appear tweets that match to the selected subject.
Each tweet that appear has � background color, determined by multiple parameters (author's followers number etc).

## Doc

https://developer.twitter.com/en/docs/tweets/filter-realtime/overview.html

## Installation

Install dependencies

```bash
npm install
```

## Run

Server will run on localhost:3000

```bash
node js/server.js
```

## How it work

When the client is connected, we lunch the stream with the filter "France, Cat, Trump". I decided not to destroy this stream during each client request.
So, we have this global stream. Each time the client click on a filter, a request is sended to the server and arriving tweets are filtered by keyword.

```javascript
// Socket IO discussion
// When server recieve "connection"
io.on('connection', function (socket) {

    // The client is connected
    // Socket emit "connected" to warm the client that connection is successfull
    socket.emit('connected', 'connected');
    socket.on('changeKeyword', function (query) {

        // When keywords is changed, we ask the client to clean up the div who contains old tweets
        socket.emit('cleanClient');
        twStream.keywordChange(query)
    });
});
```

For the background color, I created a class Color. Each time I get a twwet from the feed corresponding to the current keyword, I make a new color. The Color constructor get red, blue and green parameters. Each parameters is rounded and checked : if the value is under 0 or above 255, I fixed it to 0 or 255.
So when I pass 3 variables to the class Color, it return me an instance with 3 values (red, blue, green) I can use to create my background. So I emit a socket to the client, passing the tweet and the color.

```javascript

//When I recieve a tweet, I do :
var color = prepareColor(tweet);


function prepareColor(tweet) {
    var red = tweet.user['followers_count'];
    var blue = tweet.user['friends_count'];
    var green = tweet.user['statuses_count']/100;

    var myColor = new Color(red, blue, green);
    return myColor;
}

// Sending a socket to client, with tweet infos and color.

io.emit('tweet', { tweet: tweet, color: color });

```




