class Color{


    constructor(red, blue, green){
        this.red = this.roundNumber(red);
        this.blue = this.roundNumber(blue);
        this.green = this.roundNumber(green);
    }

    roundNumber(number){
        number = Math.round(number);
        if (number <0){
            number = 0;
        }else if(number>255){
            number = 255;
        }
        return number;
    }

}
module.exports = Color;
