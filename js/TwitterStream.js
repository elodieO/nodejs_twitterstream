//Dependencies call
const { Readable } = require('stream');
const Twitter = require ('twitter');

class TwitterStream extends Readable{

    constructor (client, query){
        console.log('trying to connect twitter');
        super({objectMode : true});
        this.client = new Twitter(client);
        this.connect(query);
        this.currentKeyword = null;
    }

    _read(){}

    connect(query){
        console.log('currently tracking ' + query);
        this.stream = this.client
            .stream('statuses/filter', {track: query});
        this.stream.on('data', tweet => this.tweetFilter(tweet, this.currentKeyword) );

    }

    // When keyword change, we call this function, to change the currentKeyword
    keywordChange(keyword) {
        this.currentKeyword = keyword;
        console.log('my new current keyword ' + this.currentKeyword);
        // this.tweetFilter(this.currentKeyword);
    }

    // To check if tweets contains keyword, and then, push the tweet to the client side
    tweetFilter(tweet, keyword){
        if (tweet.text){
            // console.log(tweet.text)
            if (tweet.text.includes(keyword)){
                this.push(tweet)
            }
        }
    }

}

module.exports = TwitterStream;
