//Dependencies call and constants
var express = require('express');
const app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
const path = require('path');
const TwitterStream = require("./TwitterStream");
const Color = require("./Color");
const {Writable} = require("stream");



// Twitter consumer informations for connection to Twitter API
const client = {
    consumer_key: 'OC4FgEwTHE8yrdP8oIuMseZ3y',
    consumer_secret: 'Fg5wCKHHhsVilGBLPQfIzgrhJO0qL7Bi8cX0hS0SxmNZft7clj',
    access_token_key: '988310018431864832-c1rnzph97ZhARsJlCYp6YOIxj5BtmZp',
    access_token_secret: 'vzqTa2NO2VPbe8PN5NZ2WveUJgPN80koTAsMiLqdSQA8p'
}

// Creation of Twiiter Stream with keywords filter
var twStream = new TwitterStream(client, 'france, cat, trump');

//Server listening on localhost port 3000
server.listen(3000);

// Link to the public repertory (css styles...)
app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(path.resolve(__dirname,'../public/index.html'));
});


// Socket IO discussion
// When server recieve "connection"
io.on('connection', function (socket) {

    // The client is connected
    // Socket emit "connected" to warm the client that connection is successfull
    socket.emit('connected', 'connected');
    socket.on('changeKeyword', function (query) {

        // When keywords is changed, we ask the client to clean up the div who contains old tweets
        socket.emit('cleanClient');
        twStream.keywordChange(query)

    });


});

const socketStream = new Writable({
    objectMode : true,
    write (tweet, _, callback){
        var color = prepareColor(tweet);
        // Emit tweet for the client side with tweet information form server
        io.emit('tweet', { tweet: tweet, color: color });
        callback();
    }
})

function prepareColor(tweet) {
    var red = tweet.user['followers_count'];
    var blue = tweet.user['friends_count'];
    var green = tweet.user['statuses_count']/100;

    var myColor = new Color(red, blue, green);
    return myColor;
}



twStream.pipe(socketStream);
